msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2006-08-12 18:13+0200\n"
"Last-Translator: Peter Karlsson <peterk@debian.org>\n"
"Language-Team: debian-l10n-swedish@lists.debian.org\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/searchtmpl/search.def:6
msgid "Search for"
msgstr "Sök efter"

#: ../../english/searchtmpl/search.def:10
msgid "Results per page"
msgstr "Resultat per sida"

#: ../../english/searchtmpl/search.def:14
msgid "Output format"
msgstr "Utdataformat"

#: ../../english/searchtmpl/search.def:18
msgid "Long"
msgstr "Långt"

#: ../../english/searchtmpl/search.def:22
msgid "Short"
msgstr "Kort"

#: ../../english/searchtmpl/search.def:26
msgid "URL"
msgstr "Adress"

#: ../../english/searchtmpl/search.def:30
msgid "Default query type"
msgstr "Sökmetod"

#: ../../english/searchtmpl/search.def:34
msgid "All Words"
msgstr "Alla ord"

#: ../../english/searchtmpl/search.def:38
msgid "Any Words"
msgstr "Något ord"

#: ../../english/searchtmpl/search.def:42
msgid "Search through"
msgstr "Sök genom"

#: ../../english/searchtmpl/search.def:46
msgid "Entire site"
msgstr "Hela webbplatsen"

#: ../../english/searchtmpl/search.def:50
msgid "Language"
msgstr "Språk"

#: ../../english/searchtmpl/search.def:54
msgid "Any"
msgstr "Alla"

#: ../../english/searchtmpl/search.def:58
msgid "Search results"
msgstr "Sökresultat"

#: ../../english/searchtmpl/search.def:65
msgid ""
"Displaying documents \\$(first)-\\$(last) of total <B>\\$(total)</B> found."
msgstr ""
"Visar dokument \\$(first)-\\$(last) av totalt <B>\\$(total)</B> träffar."

#: ../../english/searchtmpl/search.def:69
msgid ""
"Sorry, but search returned no results. <P>Some pages may be available in "
"English only, you may want to try searching again and set the Language to "
"Any."
msgstr ""
"Tyvärr, sökningen gav inga resultat. <P>Vissa sidor kan vara tillgängliga "
"endast på engelska, du kan testa att söka igen och sätta Språk till Alla."

#: ../../english/searchtmpl/search.def:73
msgid "An error occured!"
msgstr "Ett fel uppstod!"

#: ../../english/searchtmpl/search.def:77
msgid "You should give at least one word to search for."
msgstr "Du måste ange åtminstone ett ord att söka efter."

#: ../../english/searchtmpl/search.def:81
msgid "Powered by"
msgstr "Använder"

#: ../../english/searchtmpl/search.def:85
msgid "Next"
msgstr "Nästa"

#: ../../english/searchtmpl/search.def:89
msgid "Prev"
msgstr "Föregående"
