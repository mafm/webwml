# Turkish messages for Debian Web pages (debian-webwml).
# Copyright (C) 2003, 2004 Software in the Public Interest, Inc.
# This file is distributed under the same license as Debian Web pages.
#
# Murat Demirten <murat@debian.org>, 2002, 2003, 2004.
# Recai Oktaş <roktas@omu.edu.tr>, 2005.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2003-11-10 15:06+0100\n"
"PO-Revision-Date: 2006-01-18 05:45+0200\n"
"Last-Translator: Recai Oktaş <roktas@omu.edu.tr>\n"
"Language-Team: Debian L10n Turkish <debian-l10n-turkish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms:  nplurals=1; plural=0;\n"

#: ../../english/CD/vendors/vendors.CD.def:14
#, fuzzy
#| msgid "Vendor:"
msgid "Vendor"
msgstr "Satıcı:"

#: ../../english/CD/vendors/vendors.CD.def:15
#, fuzzy
#| msgid "Allows Contribution to Debian:"
msgid "Allows Contributions"
msgstr "Debian'a Katkıya İzin veriyor:"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:17
#, fuzzy
#| msgid "Architectures:"
msgid "Architectures"
msgstr "Mimariler:"

#: ../../english/CD/vendors/vendors.CD.def:18
#, fuzzy
#| msgid "Ship International:"
msgid "Ship International"
msgstr "Uluslararası "

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr ""

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
#, fuzzy
#| msgid "Vendor:"
msgid "Vendor Home"
msgstr "Satıcı:"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr ""

#: ../../english/CD/vendors/vendors.CD.def:71
#, fuzzy
#| msgid "email:"
msgid "email"
msgstr "eposta:"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "Avrupa içinde"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Bazı bölgelere"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "kaynak"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "ve"

#~ msgid "Vendor:"
#~ msgstr "Satıcı:"

#~ msgid "URL for Debian Page:"
#~ msgstr "Debian Sayfası için URL:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Debian'a Katkıya İzin veriyor:"

#~ msgid "Country:"
#~ msgstr "Ülke:"

#~ msgid "Ship International:"
#~ msgstr "Uluslararası "

#~ msgid "email:"
#~ msgstr "eposta:"

#~ msgid "CD Type:"
#~ msgstr "CD tipi:"

#~ msgid "DVD Type:"
#~ msgstr "DVD tipi:"

#~ msgid "Architectures:"
#~ msgstr "Mimariler:"

#~ msgid "updated monthly"
#~ msgstr "aylık güncelleniyor"

#~ msgid "updated twice weekly"
#~ msgstr "haftada iki defa güncelleniyor"

#~ msgid "updated weekly"
#~ msgstr "haftalık güncelleniyor"

#~ msgid "reseller"
#~ msgstr "tekrar dağıtıcı"

#~ msgid "reseller of $var"
#~ msgstr "$var için tekrar dağıtıcı"

#~ msgid "Custom Release"
#~ msgstr "Özel Sürüm"

#~ msgid "vendor additions"
#~ msgstr "satıcı eklemeleri"

#~ msgid "contrib included"
#~ msgstr "contrib dahil"

#~ msgid "non-free included"
#~ msgstr "non-free dahil"

#~ msgid "non-US included"
#~ msgstr "non-US dahil"

#~ msgid "Multiple Distribution"
#~ msgstr "Çoklu Dağıtım"

#~ msgid "Vendor Release"
#~ msgstr "Satıcı Sürümü"

#~ msgid "Development Snapshot"
#~ msgstr "Geliştirme Sürümü"

#~ msgid "Official CD"
#~ msgstr "Resmi CD"
