#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml vendors\n"
"PO-Revision-Date: 2015-06-21 08:21+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Gtranslator 2.91.7\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Nhà cung cấp"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Cho phép các phần đóng góp"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Các kiến trúc"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Vận chuyển quốc tế"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Liên hệ"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Trang chủ nhà cung cấp"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "trang"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "thư điện tử"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "bên trong châu Âu"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Đến một số vùng"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "nguồn"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "và"

#~ msgid "Vendor:"
#~ msgstr "Nhà cung cấp:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL cho trang Debian:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Cho phép các phần đóng góp cho Debian:"

#~ msgid "Country:"
#~ msgstr "Nước:"

#~ msgid "Ship International:"
#~ msgstr "Vận chuyển quốc tế:"

#~ msgid "email:"
#~ msgstr "thư điện tử:"

#~ msgid "CD Type:"
#~ msgstr "Kiểu CD:"

#~ msgid "DVD Type:"
#~ msgstr "Kiểu DVD:"

#~| msgid "CD Type:"
#~ msgid "BD Type:"
#~ msgstr "Kiểu BD:"

#~| msgid "CD Type:"
#~ msgid "USB Type:"
#~ msgstr "Kiểu USB:"

#~ msgid "Architectures:"
#~ msgstr "Các kiến trúc:"
